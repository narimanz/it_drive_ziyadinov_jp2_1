package repositories;

import models.Room;
import models.User;

import java.util.List;

public interface RoomsRepository{
    void insert(Room room,User user);
    Room getRoom(String roomName);
    List<Room> returnListMessages();
}

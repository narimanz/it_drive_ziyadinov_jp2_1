package repositories;

import lombok.SneakyThrows;
import models.Room;
import models.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class RoomRepositoryImpl implements RoomsRepository {
    private Connection connection;
    //language=SQL
    private String INSERT_USER_IN_ROOM = "insert into account_chat (account_id, chat_id) values (?,?)";
    //language=SQL
    private String SELECT_ROOM_BY_NAME = "select chat.id, chat.title from chat where title = ?";

    private RowMapper<Room> roomRowMapper = new RowMapper<Room>() {
        public Room mapRow(ResultSet row) throws SQLException {
            Integer id = row.getInt("id");
            String nameRoom = row.getString("title");
            return (new Room(id,nameRoom));
        }
    };

    public RoomRepositoryImpl(Connection connection){
        this.connection = connection;
    }

    @SneakyThrows
    public void insert(Room room, User user) {
        PreparedStatement statement = connection.prepareStatement(INSERT_USER_IN_ROOM);
        statement.setInt(1,user.getId());
        statement.setInt(2,room.getId());
        statement.execute();
        statement.close();
    }

    @SneakyThrows
    public Room getRoom(String roomName) {
        PreparedStatement statement = connection.prepareStatement(SELECT_ROOM_BY_NAME);
        statement.setString(1,roomName);
        ResultSet resultSet = statement.executeQuery();
        Room room = null;
        while (resultSet.next()){
            room = roomRowMapper.mapRow(resultSet);
        }
        return room;
    }

    public List<Room> returnListMessages() {
        return null;
    }
}

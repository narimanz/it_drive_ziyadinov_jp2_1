package ru.narimanz.sockets;

import lombok.SneakyThrows;
import models.Message;
import models.Room;
import models.User;
import repositories.RoomRepositoryImpl;
import repositories.UserRepositoryImpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;

public class MainForClient1 {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/chat-service";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "12345";

    @SneakyThrows
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        SocketClient client = new SocketClient("127.0.0.1",7777);
        Class.forName("org.postgresql.Driver");
        Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
        RoomRepositoryImpl roomRepository = new RoomRepositoryImpl(connection);
        UserRepositoryImpl userRepository = new UserRepositoryImpl(connection);
        User user;
        Room room;

        while (true){
            System.out.println("Welcome!\nEnter your nickname: ");
            String nameUser = sc.nextLine();

            user = userRepository.getUserAccount(nameUser);
            System.out.println("Hi! " + user.getNameUser());

            //AllMessageOfThisUser
//            List<Message> messageList = userRepository.getListMessages(user.getNameUser());
            //AllRoomsOfThisUser
//            List<Room> roomList = userRepository.getListRooms(user.getNameUser());


            System.out.println("Выберите комнату: ");
            String chooseRoom = sc.nextLine();
            if (chooseRoom.contains("choose room")){
                String[] roomArray = chooseRoom.split("choose room ");
                room = roomRepository.getRoom(roomArray[1]);
                roomRepository.insert(room,user);
            }

            String message = sc.nextLine();
            client.sendMessage(message);
        }
    }
}

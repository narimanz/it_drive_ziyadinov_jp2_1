package ru.narimanz.sockets;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;

public class SocketServer {
    private Socket socketClient;
    public  LinkedList<Socket> serverList = new LinkedList();


    public void startServer(int port){
        java.net.ServerSocket serverSocket = null;
        try {
            serverSocket = new java.net.ServerSocket(port);
        }catch (IOException e){
            throw new IllegalArgumentException(e);
        }

        while (true){
            try {
                socketClient = serverSocket.accept();
                serverList.add(socketClient);
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
            new ServerThread(socketClient).start();
        }
    }

    private class ServerThread extends Thread{
        private Socket socket;

        public ServerThread(Socket socket){
            this.socket = socket;
        }

        public void run(){
            try {
                InputStream clientInputStream = socket.getInputStream();
                BufferedReader clientReader = new BufferedReader(new InputStreamReader(clientInputStream));
                String inputLine = clientReader.readLine();
                while (inputLine != null){
                    for (Socket socket : serverList) {
                        PrintWriter writer = new PrintWriter(socket.getOutputStream(),true);
                        writer.println(inputLine);
                    }
                    inputLine = clientReader.readLine();
                    System.out.println(Thread.currentThread().getName());
                }
            } catch (IOException e) {
                throw  new IllegalArgumentException(e);
            }
        }
    }

}
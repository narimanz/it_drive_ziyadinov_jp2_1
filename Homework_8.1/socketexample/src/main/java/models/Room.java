package models;

import lombok.*;

import java.util.Date;
import java.util.List;


@Getter
@Setter
public class Room {
    private Integer id;
    private String title;
    private List<User> members; // члены комнаты
    private List<Message> messages; // все сообщения в данной комнате

    public Room(Integer id, String title) {
        this.id = id;
        this.title = title;
    }

    public Room(String title) {
        this.title = title;
    }
}

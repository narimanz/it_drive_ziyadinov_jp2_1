package models;

import lombok.*;

import java.util.Date;



@Getter
@Setter
public class Message {
    private String textMessage;
    private Date publishDate;
    private User author; // автор сообщения
    private Room room; // комната сообщения



    public Message(String textMessage, User author, Room room) {
        this.textMessage = textMessage;
        this.author = author;
        this.room = room;
    }

    public Message(String textMessage) {
        this.textMessage = textMessage;
    }

}

package models;

import lombok.*;

import java.util.ArrayList;
import java.util.List;


@Setter
@Getter
public class User {
    private Integer id;
    private String nameUser;


    List<Room> rooms = new ArrayList<Room>(); // список комнат данного пользователя
    List<Message> messages = new ArrayList<Message>(); // список его сообщений

    public User(Integer id, String nameUser) {
        this.id = id;
        this.nameUser = nameUser;
    }

    public User(String nameUser) {
        this.nameUser = nameUser;
    }

    public User(){}
}

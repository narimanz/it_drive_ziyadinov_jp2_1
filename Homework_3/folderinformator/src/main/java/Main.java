package main.java;

import com.beust.jcommander.JCommander;
import ru.narimanz.Folder;
import ru.narimanz.FolderApi;

import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        FolderApi folderApi = new FolderApi();

        Argument argument = new Argument();
        JCommander.newBuilder()
                .addObject(argument)
                .build()
                .parse(args);

        //folderApi.getFileInformationInFolder("E:\\test");

        for (Map.Entry<String, List<Folder>> s : folderApi
                .getFileInformationInFolder(argument.url)
                .entrySet()) {
            for(Folder item : s.getValue()){
                System.out.println(s.getKey() + " " + item.getFileName() + " " + item.getFileSize());
            }
        }
    }
}

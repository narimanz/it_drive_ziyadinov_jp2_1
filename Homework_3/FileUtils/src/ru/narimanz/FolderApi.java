package ru.narimanz;

import java.io.File;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FolderApi extends Folder{

    private HashMap<String,List<Folder>> fileInformation = new HashMap<>();


    public FolderApi() {

    }


    public Map <String, List<Folder>> getFileInformationInFolder(String folderDirectory){
        File directory = new File(folderDirectory);
        ExecutorService service = Executors.newFixedThreadPool(directory.listFiles().length);
        for (File itemDerectory : directory.listFiles()){
            List<Folder> filesInDerectoty = new ArrayList<>();
            if (itemDerectory.isDirectory()){
                service.execute(new Runnable() {
                    @Override
                    public void run() {
                        File file = new File(itemDerectory.getPath());
                        for(File itemFile : file.listFiles()){
                            filesInDerectoty.add(new Folder(itemFile.getName(),itemFile.length()));
                        }
                        fileInformation.put(itemDerectory.getName(),filesInDerectoty);
                        System.out.println("Поток: " + Thread.currentThread().getName() + ". Папка: " + itemDerectory.getName());
                    }
                });
            }
        }
        service.shutdown();
        return fileInformation;
    }


}

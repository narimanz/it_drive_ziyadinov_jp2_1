package ru.narimanz;

public class Folder {
    private String fileName;
    private long fileSize;

    public Folder(String fileName, long fileSize) {
        this.fileName = fileName;
        this.fileSize = fileSize;
    }

    public Folder() {
    }


    public String getFileName() {
        return fileName;
    }

    public long getFileSize() {
        return fileSize;
    }

}

package ru.narimanz;

import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
	    FolderApi folderApi = new FolderApi();

	    folderApi.getFileInformationInFolder("E:\\test");

        for (Map.Entry<String, List<Folder>> s : folderApi
                .getFileInformationInFolder("E:\\test")
                .entrySet()) {
            for(Folder item : s.getValue()){
                System.out.println(s.getKey() + " " + item.getFileName() + " " + item.getFileSize());
            }
        }
    }
}

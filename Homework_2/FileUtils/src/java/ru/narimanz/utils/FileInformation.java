package ru.narimanz.utils; 

import java.util.ArrayList;
import java.util.List;

public class FileInformation{

    private String fileName;
    private long fileSize;
    public List<FileInformation> filesInformationInFolder = new ArrayList<>();
    
	public String getFileName() {
        return fileName;
    }

    public long getFileSize() {
        return fileSize;
    }


    public FileInformation(String fileName, long fileSize) {
        this.fileName = fileName;
        this.fileSize = fileSize;
    }
    public FileInformation(){}

}

create table account
(
    id serial primary key,
    first_name char(20),
    last_name char(20),
    mail char(20),
    pass varchar(255)
);

create table chat
(
    id serial primary key,
    title char(20),
    create_date timestamp
);

create table message
(
    id serial primary key,
    text char(255),
    account_id integer,
    chat_id integer,
    publish_date timestamp,
    foreign key (account_id) references account(id),
    foreign key (chat_id) references chat(id)
);

create table account_chat
(
    account_id integer,
    chat_id integer,
    foreign key (account_id) references account(id),
    foreign key (chat_id) references chat(id)
);

create table account_message
(
  account_id integer,
  message_id integer,
  foreign key (account_id) references  account(id),
  foreign key (message_id) references  message(id)
);
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import services.MessagesService;
import sockets.SocketServer;

public class MainServer {
    public static void main(String[] args) {
        SocketServer socketServer = new SocketServer();
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");

        socketServer.startServer(7777, context);
    }
}

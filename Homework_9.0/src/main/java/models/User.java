package models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User {
    private Integer id;
    private String nameUser;


    List<Room> rooms = new ArrayList<Room>(); // список комнат данного пользователя
    List<Message> messages = new ArrayList<Message>(); // список его сообщений


    public User(Integer id, String nameUser){
        this.id = id;
        this.nameUser = nameUser;
    }
}

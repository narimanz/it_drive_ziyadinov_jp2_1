package models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Random;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Room {
    private Integer id;
    private String title;
    private List<User> members; // члены комнаты
    private List<Message> messages; // все сообщения в данной комнате

    public Room(Integer id, String title){
        this.id = id;
        this.title = title;
    }

    public Room(String title){
        this.title = title;
    }
}

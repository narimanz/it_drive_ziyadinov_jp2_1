package models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Message {
    private String textMessage;
    private LocalDateTime publishDate;
    private User author; // автор сообщения
    private Room room; // комната сообщения

    public Message(String textMessage){
        this.textMessage = textMessage;
    }
}

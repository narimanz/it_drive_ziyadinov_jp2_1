package sockets;

import models.Room;
import org.springframework.context.ApplicationContext;
import services.MessagesService;
import services.RoomService;
import services.UsersService;

import java.io.*;
import java.net.Socket;
import java.util.*;

public class SocketServer {

    private List<ChatSocketClient> anonyms;
    private Map<Long, List<ChatSocketClient>> users;
    private MessagesService messagesService;
    private RoomService roomService;
    private UsersService usersService;
    private Socket socketClient;




    private String chooseRoomCommand = "choose room";
    private String exitRoomCommand = "exit room";
    private String authorizeCommand = "auth";

    public void startServer(int port, ApplicationContext context){
        java.net.ServerSocket serverSocket = null;
        anonyms = new ArrayList<>();
        users = new HashMap<>();
        messagesService = context.getBean(MessagesService.class);
        usersService = context.getBean(UsersService.class);
        roomService = context.getBean(RoomService.class);
        try {
            serverSocket = new java.net.ServerSocket(port);
        }catch (IOException e){
            throw new IllegalArgumentException(e);
        }

        while (true){
            try {
                socketClient = serverSocket.accept();
                anonyms.add(new ChatSocketClient(socketClient));

                //Если всё таки введёт свои данные
                //serverList.add(socketClient);
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
            new ChatSocketClient(socketClient).start();
        }
    }

    private class ChatSocketClient extends Thread{
        private Long userId; // id-пользователя
        private Long roomId; // id-текущей комнаты пользователя
        private Socket socket; // объектная переменная для взаимодействия с пользователям в рамках протокола сокетов.
        private BufferedReader input; // входной поток для чтения символов от сокета-клиента
        private PrintWriter output; // выходной поток для записи символов от сокета-сервера сокету-клиенту

        public ChatSocketClient(Socket socket){
            this.socket = socket;
        }


        public void run(){
            try {
                InputStream clientInputStream = socket.getInputStream();
                BufferedReader clientReader = new BufferedReader(new InputStreamReader(clientInputStream));
                String inputLine = clientReader.readLine();
                while (inputLine != null){


                    //ВХОД, ВВОД НИКА
                    if (inputLine.contains(authorizeCommand)){
                        String [] wordsAuth = inputLine.split(" ");
                        String accountName = wordsAuth[1];
                        userId = Long.parseLong(String.valueOf(usersService.getUser(accountName).getId()));

                        //Ввод комнаты
                        if(inputLine.contains(chooseRoomCommand)){
                            // открываем комнату выгружаем 30 сообщений
                            // записываем

                            String [] wordsChooseRoom = inputLine.split(" ");
                            String roomName = wordsChooseRoom[2];
                            Room room = roomService.getRoom(roomName);
                            roomId = Long.valueOf(String.valueOf(room.getId()));

                            if (users.containsKey(roomId)){
                                users.get(roomId).add(new ChatSocketClient(socketClient));
                            }
                            else{
                                List<ChatSocketClient> clients = new ArrayList<>();
                                clients.add(new ChatSocketClient(socketClient));
                                users.put(roomId,clients);
                            }
                        }

                        if (inputLine.contains(exitRoomCommand)){
                            // выходим из комнаты
                        }

                        if (!inputLine.contains(chooseRoomCommand) ||
                                !inputLine.contains(exitRoomCommand)||
                                !inputLine.contains(authorizeCommand)){
                            // если просто сообщение, записываешь в бд и делаешь рассылку по всем пользователям

                            for (ChatSocketClient chatSocketClient : users.get(roomId)) {
                                PrintWriter writer = new PrintWriter(chatSocketClient.socket.getOutputStream(),true);
                                writer.println(inputLine);
                            }
                            inputLine = clientReader.readLine();
                            System.out.println(Thread.currentThread().getName());
                        }

                    }


                }
            } catch (IOException e) {
                throw  new IllegalArgumentException(e);
            }
        }
    }
}

package repositories;

import models.Message;

import java.sql.Date;

public interface MessagesRepository {
    void save(Message message);
}

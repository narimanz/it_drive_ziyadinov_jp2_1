package repositories;

import models.Message;
import models.Room;
import models.User;

import java.util.List;

public interface UsersRepository {
    public User getUserAccount(String userName);
    public List<Message> getListMessages(String userName);
    public List<Room> getListRooms(String userName);
}

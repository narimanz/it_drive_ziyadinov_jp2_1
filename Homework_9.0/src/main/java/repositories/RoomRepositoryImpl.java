package repositories;

import lombok.SneakyThrows;
import models.Room;
import models.User;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class RoomRepositoryImpl {
    private DataSource dataSource;
    //language=SQL
    private String INSERT_USER_IN_ROOM = "insert into account_chat (account_id, chat_id) values (?,?)";
    //language=SQL
    private String SELECT_ROOM_BY_NAME = "select chat.id, chat.title from chat where title = ?";
    //language=SQL
    private String SELECT_LAST_30_MESSAGE = "select message.text from message\n" +
            "    inner join chat c on message.chat_id = c.id where title = '?'\n" +
            "order by message.id desc\n" +
            "LIMIT ?";

    private RowMapper<Room> roomRowMapper = new RowMapper<Room>() {
        public Room mapRow(ResultSet row) throws SQLException {
            Integer id = row.getInt("id");
            String nameRoom = row.getString("title");
            return (new Room(id,nameRoom));
        }
    };

    public RoomRepositoryImpl(DataSource dataSource){
        this.dataSource = dataSource;
    }

    @SneakyThrows
    public void insert(Room room, User user) {
        PreparedStatement statement = dataSource.getConnection().prepareStatement(INSERT_USER_IN_ROOM);
        statement.setInt(1,user.getId());
        statement.setInt(2,room.getId());
        statement.execute();
        statement.close();
    }

    @SneakyThrows
    public Room getRoom(String roomName) {
        PreparedStatement statement = dataSource.getConnection().prepareStatement(SELECT_ROOM_BY_NAME);
        statement.setString(1,roomName);
        ResultSet resultSet = statement.executeQuery();
        Room room = null;
        while (resultSet.next()){
            room = roomRowMapper.mapRow(resultSet);
        }
        return room;
    }

    public List<Room> returnListMessages() {
        return null;
    }
}

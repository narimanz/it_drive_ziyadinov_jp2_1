package repositories;

import lombok.SneakyThrows;
import models.Message;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;

public class MessageRepositoryImpl implements MessagesRepository {
    private PreparedStatement preparedStatement;
    private DataSource dataSource;

    //language=SQL
    private static String SQL_INSERT_MESSAGE = "insert into message(text, account_id, chat_id, publish_date) VALUES (?,?,?,?)";

    public MessageRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    @SneakyThrows
    public void save(Message message) {
        Connection connection = dataSource.getConnection();
        preparedStatement = connection.prepareStatement(SQL_INSERT_MESSAGE);
        preparedStatement.setString(1, message.getTextMessage());
        preparedStatement.setInt(2,message.getAuthor().getId());
        preparedStatement.setLong(3, message.getRoom().getId());
        preparedStatement.setTimestamp(4, Timestamp.valueOf(message.getPublishDate()));


        int affectedRows = preparedStatement.executeUpdate();

        preparedStatement.close();

        if (affectedRows != 1){
            throw new IllegalStateException("Message save not executed");
        }

    }
}

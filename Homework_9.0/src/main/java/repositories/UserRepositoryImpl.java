package repositories;

import lombok.SneakyThrows;
import models.Message;
import models.Room;
import models.User;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class UserRepositoryImpl {
    //language=SQL
    private String SQL_SELECT_BY_Name = "SELECT account.id, account.first_name from account where account.first_name = ?";
    //language=SQL
    private String SQL_SELECT_ALL_MESSAGES = "select m.text  from account\n" +
            "    inner join message m on account.id = m.account_id\n" +
            "where first_name = ?";
    //language=SQL
    private String SQL_SELECT_ALL_ROOMS = "select chat.title from chat\n" +
            "    inner join account_chat ac on chat.id = ac.chat_id\n" +
            "    inner join account a on ac.account_id = a.id\n" +
            "where first_name = ?";

    //language=SQL
    private String SQL_INSERT_NEW_USER = "insert into account (first_name) VALUES (?)";

    private DataSource dataSource;
    private PreparedStatement statement;
    private User user;


    private RowMapper<User> userRowMapper = new RowMapper<User>() {
        public User mapRow(ResultSet row) throws SQLException {
            Integer userId = row.getInt("id");
            String nameUser = row.getString("first_name");

            return (new User(userId,nameUser));
        }
    };

    private RowMapper<User> messageRowMapper = new RowMapper<User>() {
        public User mapRow(ResultSet row) throws SQLException {

            String  text = row.getString("text");
            user.getMessages().add(new Message(text));

            return user;
        }
    };

    private RowMapper<User> roomRowMapper = new RowMapper<User>() {
        public User mapRow(ResultSet row) throws SQLException {

            String title = row.getString("title");
            user.getRooms().add(new Room(title));

            return user;
        }
    };

    public UserRepositoryImpl(DataSource dataSource){
        this.dataSource = dataSource;
    }

    @SneakyThrows
    public User getUserAccount(String userName) {
        statement = dataSource.getConnection().prepareStatement(SQL_SELECT_BY_Name);
        statement.setString(1,userName);
        ResultSet resultSet = statement.executeQuery();

        if (resultSet == null){
            statement = dataSource.getConnection().prepareStatement(SQL_INSERT_NEW_USER);
            statement.setString(1,userName);
            statement.executeQuery();
            getUserAccount(userName);
        }

        while (resultSet.next()){
            user = userRowMapper.mapRow(resultSet);
        }
        statement.close();
        resultSet.close();

        return user;
    }

    @SneakyThrows
    public List<Message> getListMessages(String userName) {
        statement = dataSource.getConnection().prepareStatement(SQL_SELECT_ALL_MESSAGES);
        statement.setString(1,userName);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()){
            messageRowMapper.mapRow(resultSet);
        }
        statement.close();
        resultSet.close();

        return user.getMessages();
    }

    @SneakyThrows
    public List<Room> getListRooms(String userName) {
        statement = dataSource.getConnection().prepareStatement(SQL_SELECT_ALL_ROOMS);
        statement.setString(1,userName);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()){
            roomRowMapper.mapRow(resultSet);
        }
        statement.close();
        resultSet.close();

        return user.getRooms();
    }
}

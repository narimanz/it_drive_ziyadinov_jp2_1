package services;

import models.Room;
import repositories.RoomRepositoryImpl;
import repositories.RoomsRepository;

public class RoomService {
    private RoomRepositoryImpl roomsRepository;

    public RoomService(RoomRepositoryImpl roomsRepository) {
        this.roomsRepository = roomsRepository;
    }

    public Room getRoom(String roomName){
        return roomsRepository.getRoom(roomName);
    }
}

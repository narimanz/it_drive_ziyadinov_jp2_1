package services;

import models.Message;
import repositories.MessageRepositoryImpl;


public class MessagesService {
    private MessageRepositoryImpl messageRepository;

    public MessagesService(MessageRepositoryImpl messageRepository) {
        this.messageRepository = messageRepository;
    }

    public void sendMessage(Message message){
        messageRepository.save(message);
    }

}

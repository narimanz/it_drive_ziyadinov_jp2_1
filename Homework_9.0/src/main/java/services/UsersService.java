package services;

import models.User;
import repositories.UserRepositoryImpl;

public class UsersService {
    private UserRepositoryImpl userRepository;

    public UsersService (UserRepositoryImpl userRepository){
        this.userRepository = userRepository;
    }

    public User getUser(String name){
        return userRepository.getUserAccount(name);
    }
}

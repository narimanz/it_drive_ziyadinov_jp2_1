package ru.narimanz.utils; 

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileInformationApi extends FileInformation{

	public ArrayList<FileInformation> getFileInformation(String filePath){

        File directory = new File(filePath);
        if (directory.isDirectory()){
				for(File item : directory.listFiles()){
                 	if(item.isFile()){
                     	filesInformationInFolder.add(new FileInformation(item.getName(),item.length()));
             		}
				}
				return (ArrayList<FileInformation>) filesInformationInFolder;
        }

        return null;
    }

}
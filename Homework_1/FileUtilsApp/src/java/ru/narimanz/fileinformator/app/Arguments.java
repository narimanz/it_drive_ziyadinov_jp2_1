package ru.narimanz.fileinformator.app;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
class Arguments{

	@Parameter(names = {"--url"})
	public String url;
}
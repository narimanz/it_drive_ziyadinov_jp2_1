package ru.narimanz.fileinformator.app;

import java.io.File;
import com.beust.jcommander.JCommander;
import target.ru.narimanz.utils.*;

class Program{
	public static void main(String[] args) {


		Arguments arguments = new Arguments();
		JCommander.newBuilder()
			.addObject(arguments)
			.build()
			.parse(args);

		//FileInformationApi fileinf = new FileInformationApi();
        for(FileInformation item: new FileInformationApi().getFileInformation(arguments.url)){
            System.out.println("File name "+ (item.getFileName()) + ", file size: " + item.getFileSize());
        }
	}
}
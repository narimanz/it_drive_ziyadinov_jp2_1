package ru.narimanz;

import java.util.Deque;
import java.util.LinkedList;

public class ThreadPool {

    private PoolWorker threads[];

    private Deque<Runnable> tasks;

    public ThreadPool(int threadsCount) {
        this.tasks = new LinkedList<>();
        this.threads = new PoolWorker[threadsCount];
        for (int i = 0; i < threads.length; i++) {
            this.threads[i] = new PoolWorker();
            this.threads[i].start();
        }
    }

    public void submit(Runnable task) {
        synchronized (tasks) {
            tasks.add(task);
            tasks.notify();
        }
    }

    private class PoolWorker extends Thread {
        @Override
        public void run() {
            Runnable task;

            while (true){
                synchronized (tasks){
                    while (tasks.isEmpty()){
                        try {
                            tasks.wait();
                        } catch (InterruptedException e) {
                            throw new IllegalArgumentException(e);
                        }
                    }

                    //System.out.println(tasks.getFirst());
                    task = tasks.pollFirst();
                    //System.out.println(tasks.getFirst() + "AAAAAA");
                }
                task.run();
            }

        }
    }
}

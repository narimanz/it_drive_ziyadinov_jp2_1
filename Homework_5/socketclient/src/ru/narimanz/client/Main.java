package ru.narimanz.client;

import com.beust.jcommander.JCommander;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        Argument argument = new Argument();
        JCommander.newBuilder()
                .addObject(argument)
                .build()
                .parse(args);

        SocketClient client = new SocketClient(argument.serverHost,argument.serverPort);

        while (true){
            String message = sc.nextLine();
            client.sendMessage(message);
        }

    }

}

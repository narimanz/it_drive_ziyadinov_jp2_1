package ru.narimanz.client;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class Argument {
    @Parameter(names = {"--serverHost"})
    public String serverHost;

    @Parameter(names = {"--serverPort"})
    public int serverPort;

}

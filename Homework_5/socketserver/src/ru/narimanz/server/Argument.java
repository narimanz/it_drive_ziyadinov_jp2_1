package ru.narimanz.server;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class Argument {
    @Parameter(names = {"--port"})
    public int port;
}
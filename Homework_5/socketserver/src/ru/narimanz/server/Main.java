package ru.narimanz.server;

import com.beust.jcommander.JCommander;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {

        Argument argument = new Argument();
        JCommander.newBuilder()
                .addObject(argument)
                .build()
                .parse(args);

        SocketServer socketServer = new SocketServer();
        socketServer.startServer(argument.port);
    }
}

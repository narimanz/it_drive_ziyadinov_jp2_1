package ru.narimanz.jdbc.repositories;

import ru.narimanz.jdbc.models.Lesson;

public interface LessonsRepositrory extends CrudRepository<Lesson> {
}

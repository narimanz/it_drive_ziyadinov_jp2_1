package ru.narimanz.jdbc.models;

import java.sql.Timestamp;
import java.util.List;

public class Course {

    private Integer id;
    private String courseTitle;
    private Timestamp startDate;
    private Timestamp finishDate;

    private List<Lesson> listOfLessonsInCourse;
    private List<String> lessonList;

    public List<Lesson> getListOfLessonsInCourse() {
        return listOfLessonsInCourse;
    }

    public Course(String courseTitle, Timestamp startDate, Timestamp finishDate, List<Lesson> listOfLessonsInCourse) {
        this.id = id;
        this.courseTitle = courseTitle;
        this.startDate = startDate;
        this.finishDate = finishDate;
        this.listOfLessonsInCourse = listOfLessonsInCourse;
    }

    public Course(Integer id, String title, Timestamp startDate, Timestamp finishDate) {
        this.id = id;
        this.courseTitle = title;
        this.startDate = startDate;
        this.finishDate = finishDate;
    }

    public Course(String courseTitle, List<Lesson> lessonList) {
        this.courseTitle = courseTitle;
        this.listOfLessonsInCourse = lessonList;
    }

    public Course(String courseTitle) {
        this.courseTitle = courseTitle;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return courseTitle;
    }

    public void setTitle(String title) {
        this.courseTitle = title;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Timestamp finishDate) {
        this.finishDate = finishDate;
    }


    @Override
    public String toString() {
        String param = "Course{"+
                " title='" + courseTitle + '\'' +
                ", startDate=" + startDate +
                ", finishDate=" + finishDate +
                ", lessons = " + listOfLessonsInCourse +
                '}';
        String param1 = "Course{"+
                " title='" + courseTitle + '\'' +
                ", lessons = " + listOfLessonsInCourse +
                '}';
        String param2 = "Course{"+
                " title='" + courseTitle + '\'' +
                '}';
        if (startDate == null && listOfLessonsInCourse!=null)
            return param1;
        else if(listOfLessonsInCourse==null)
            return param2;
        else return param;
    }


}

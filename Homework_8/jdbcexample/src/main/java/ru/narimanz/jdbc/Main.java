package ru.narimanz.jdbc;

import ru.narimanz.jdbc.models.Course;
import ru.narimanz.jdbc.models.Lesson;
import ru.narimanz.jdbc.repositories.CoursesRepository;
import ru.narimanz.jdbc.repositories.CoursesRepositoryImpl;
import ru.narimanz.jdbc.repositories.LessonsRepositrory;
import ru.narimanz.jdbc.repositories.LessonsRepositroryImpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class Main {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/education_center";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "12345";

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

        CoursesRepository coursesRepository = new CoursesRepositoryImpl(connection);
        LessonsRepositrory lessonsRepositrory = new LessonsRepositroryImpl(connection);


        //READY
//        Course course = coursesRepository.find(2);
//        System.out.println(course);

        //READY
//        Lesson lesson = lessonsRepositrory.find(3);
//        System.out.println(lesson);

        ///System.out.println("-------");

        //READY
//        List<Lesson> listOfAllLessons = lessonsRepositrory.findAll();
//        System.out.println(listOfAllLessons);

        // READY
//        List<Course> listOfAllCourses = coursesRepository.findAll();
//        System.out.println(listOfAllCourses);
    }
}

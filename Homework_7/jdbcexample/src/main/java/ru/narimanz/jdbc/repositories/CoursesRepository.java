package ru.narimanz.jdbc.repositories;

import ru.narimanz.jdbc.models.Course;

public interface CoursesRepository extends CrudRepository<Course> {
}

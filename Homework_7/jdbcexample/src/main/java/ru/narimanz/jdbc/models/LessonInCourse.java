package ru.narimanz.jdbc.models;

public class LessonInCourse {
    private String title;
    private String name;

    public LessonInCourse(String title, String name) {
        this.title = title;
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "LessonInCourse{" +
                "title='" + title + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}

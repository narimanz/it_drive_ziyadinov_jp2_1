package ru.narimanz.jdbc.models;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class Lesson {
    private Integer id;
    private String courseTitle;
    private String lessonName;
    private ArrayList<Course> courseArrayList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Lesson(String courseTitle, String lessonName) {
        this.courseTitle = courseTitle;
        this.lessonName = lessonName;
    }

    public Lesson(Integer id, String lessonName) {
        this.id = id;
        this.lessonName = lessonName;
    }

    public Lesson(String lessonName) {
        this.lessonName = lessonName;
    }

    public Lesson(String lessonName, ArrayList<Course> courseArrayList) {
        this.lessonName = lessonName;
        this.courseArrayList = courseArrayList;
    }

    public String getCourseTitle() {
        return courseTitle;
    }

    public void setCourseTitle(String courseTitle) {
        this.courseTitle = courseTitle;
    }

    public String getLessonName() {
        return lessonName;
    }

    public void setLessonName(String lessonName) {
        this.lessonName = lessonName;
    }

    public ArrayList<Course> getCourseArrayList() {
        return courseArrayList;
    }

    public void setCourseArrayList(ArrayList<Course> courseArrayList) {
        this.courseArrayList = courseArrayList;
    }

    @Override
    public String toString() {
        String param1 = "Lesson{" +
                "id='" + id + '\'' +
                ", lessonName='" + lessonName + '\'' +
                '}';

        String param2 = "Lesson{" +
                "courseTitle='" + courseTitle + '\'' +
                ", lessonName='" + lessonName + '\'' +
                '}';
        String param3 = "Lesson{" +
                "lessonName='" + lessonName + '\'' +
                "course='" + courseArrayList + '\''+
                '}';

        if (id != null) return param1;
        else if (courseTitle == null) return param3;
        else return param2;
    }

}

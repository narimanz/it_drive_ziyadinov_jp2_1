package ru.narimanz.jdbc.repositories;

import ru.narimanz.jdbc.models.Course;
import ru.narimanz.jdbc.models.Lesson;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LessonsRepositroryImpl implements LessonsRepositrory {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select lesson.id, lesson.name, title from lesson inner join course c on lesson.course_id = c.id where course_id = lesson.course_id";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select lesson.name, title from lesson inner join course c on lesson.course_id = c.id where lesson.id =  ";

    private Connection connection;

    Map<Integer, Lesson> map = new HashMap<>();

    private RowMapper<Lesson> courseRowMapper = new RowMapper<Lesson>() {
        public Lesson mapRow(ResultSet row) throws SQLException {
            return new Lesson(
                    row.getString("title"),
                    row.getString("name")
            );
        }
    };

    private RowMapper<Lesson> lessonInCourseRowMapper = new RowMapper<Lesson>() {
        public Lesson mapRow(ResultSet row) throws SQLException {
            Integer lessonId = row.getInt("id");
            if (!map.containsKey(lessonId)){
                Lesson lesson = new Lesson(row.getString("name"), new ArrayList<Course>());
                map.put(lessonId,lesson);
            }
            Course course = new Course(row.getString("title"));
            map.get(lessonId).getCourseArrayList().add(course);
            return null;
        }
    };

    public LessonsRepositroryImpl(Connection connection) {
        this.connection = connection;
    }


    @Override
    public void save(Lesson object) {

    }

    @Override
    public void update(Lesson object) {

    }

    public void delete(Integer id) {

    }

    public Lesson find(Integer id) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_BY_ID + id);
            resultSet.next();
            Lesson lessonInCourse = courseRowMapper.mapRow(resultSet);
            return lessonInCourse;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }



    public List<Lesson>findAll() {
        try {
            List<Lesson> result = new ArrayList<Lesson>();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL);

            while (resultSet.next()){
                lessonInCourseRowMapper.mapRow(resultSet);
            }
            for (Map.Entry<Integer,Lesson> lessonEntry: map.entrySet()){
                result.add(new Lesson(lessonEntry.getValue().getLessonName(),lessonEntry.getValue().getCourseArrayList()));
            }

            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}

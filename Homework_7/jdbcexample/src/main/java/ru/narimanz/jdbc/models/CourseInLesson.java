package ru.narimanz.jdbc.models;

import java.util.List;

public class CourseInLesson {
    private String name;
    private String title;
    private List<String> allLessonsInCourse;

    public CourseInLesson(String name, String title) {
        this.name = name;
        this.title = title;
    }

    public CourseInLesson(String name, List<String> allLessonsInCourse) {
        this.name = name;
        this.allLessonsInCourse = allLessonsInCourse;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "CourseInLesson{" +
                "title='" + name + '\'' +
                ", nameLessons='" + allLessonsInCourse + '\'' +
                '}';
    }

}

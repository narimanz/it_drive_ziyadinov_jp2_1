package ru.narimanz.jdbc.repositories;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;
import ru.narimanz.jdbc.models.Course;
import ru.narimanz.jdbc.models.Lesson;

import java.sql.*;
import java.util.*;

public class CoursesRepositoryImpl implements CoursesRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from course left join lesson l on course.id = l.course_id";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from lesson inner join course c on lesson.course_id = c.id where c.id = ";

    private Connection connection;

    Map<Integer, Course> map = new HashMap<>();

    private RowMapper<Course> lessonRowMapper = new RowMapper<Course>() {
        public Course mapRow(ResultSet row) throws SQLException {
            Integer courseId = row.getInt("course_id");
            // если в мапе еще не было такого курса
            if (!map.containsKey(courseId)) {
                // создаешь курс
                Course course = new Course(row.getString("title"), new ArrayList<Lesson>());
                // кладешь в мапу
                map.put(courseId, course);
            }
            // создаешь урок
            Lesson lesson = new Lesson(row.getString("name"));
            // получаешь курс этого урока и кладешь в этот курс этот урок в список
            map.get(courseId).getListOfLessonsInCourse().add(lesson);

            return null;
        }
    };

    private RowMapper<Lesson> courseRowMapper = new RowMapper<Lesson>() {
        public Lesson mapRow(ResultSet row) throws SQLException {
            return new Lesson(
                row.getString("name")
            );
        }
    };



    public CoursesRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    public void save(Course object) {

    }

    public void update(Course object) {

    }

    public void delete(Integer id) {

    }

    public Course find(Integer id) {
        try {
            String titleName=null;
            Timestamp startDate=null;
            Timestamp finishDate=null;
            //  создал список уроков пустой
            List<Lesson> listOfLessonsInCourse = new ArrayList<Lesson>();
            // создаешь стейтмент
            Statement statement = connection.createStatement();
            // выполнил запрос
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_BY_ID + id);
            // пока есть что-то в резалт сет
            while (resultSet.next()){
                // получаешь урок
                Lesson lessons = courseRowMapper.mapRow(resultSet);
                // добавляешь в список
                listOfLessonsInCourse.add(lessons);
                // данные курса
                titleName = resultSet.getString("title");
                startDate = resultSet.getTimestamp("start_date");
                finishDate = resultSet.getTimestamp("finish_date");
            }
            return (new Course(titleName,startDate,finishDate,listOfLessonsInCourse));
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }


    public List<Course> findAll() {
        try {
            // список курсов
            List<Course> result = new ArrayList<Course>();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL);
            // мапа - ключ - название курса, значение - список название предметов

            while (resultSet.next()){
                lessonRowMapper.mapRow(resultSet);
            }
            for (Map.Entry<Integer,Course> courseEntry: map.entrySet()){
                result.add(new Course(courseEntry.getValue().getTitle(), courseEntry.getValue().getListOfLessonsInCourse()));
            }

            return result;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
